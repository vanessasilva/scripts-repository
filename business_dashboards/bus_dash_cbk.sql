WITH 
gmv_base as (
    SELECT DISTINCT
         date(CAST(create_datetime AS timestamp)) create_date
       , count(DISTINCT order_id) orders
       , CAST(sum(gmv) AS decimal(38,2)) gmv
       , CAST(sum(case when order_be_status_id in (2,4,11,12,13,14,15) then gmv else 0 end)AS decimal(38,2)) as nmv
       , cast(sum(case when payment_method_id = 1 then gmv else null end) as decimal(38,2)) as gmv_cc
       , cast(sum(case when payment_method_id = 1 and order_be_status_id in (2,4,11,12,13,14,15) then gmv else null end) as decimal(38,2)) as nmv_cc
       , count(distinct(case when payment_method_id = 1 then order_id else null end)) as orders_cc
   FROM shopee_br.order_mart_dwd_order_all_event_final_status_df
   GROUP BY 1
)
, payments as ( 
    select distinct 
        p.transaction_id
        , date(c.chargeback_date) as chargeback_date
        , cast(amount_chargeback as decimal(20,2)) as chargeback_amount
        , c.description as chargeback_description
        --, c.dispute_s_status as chargeback_status
        , c.credit_card_brand
    from  brbi_opspay.ebanx_chargebacks c
        left join shopee_br.shopee_payment_module_payment_br_db__payment_v2_tab p on c.pmt_hash = p.channel_ref -- a ordem dos join faz diferença 
    order by 2 desc
    ) 
, orders as ( --a granularidade das subqueries deve ser a mesma: cbk em nivel checkout e orders em nivel order 
    select distinct 
        o.checkout_id
        , o.payment_transaction_id
        , date(cast(o.create_datetime as timestamp)) checkout_date
        , cast(sum(o.gmv) as decimal(20,2)) as gmv 
        , o.payment_be_channel as payment_channel
        , o.payment_method_id
        , count(distinct o.order_id) total_orders
    from shopee_br.order_mart_dwd_order_all_event_final_status_df o
        group by 1, 2, 3, 5, 6
    order by 3 desc
    ) 
, dim as (
    select distinct 
        o.checkout_id
        , o.payment_transaction_id
        , o.checkout_date
        , o.gmv
        , p.chargeback_date
        , p.chargeback_amount
        , o.payment_channel
        , o.payment_method_id
        , o.total_orders
        , p.chargeback_description
        --, p.dispute_s_status as chargeback_status
        , p.credit_card_brand
    from orders o
        left join payments p on p.transaction_id = o.payment_transaction_id
    order by 3 desc 
    ) 
, totals as (
    select distinct 
        checkout_date
        , chargeback_date
        , chargeback_description
        -- , chargeback_status
        , credit_card_brand
        , count(DISTINCT checkout_id) num_cc_checkout
        , sum(gmv) cc_gmv
        , sum(total_orders) as num_cc_orders
        , count(distinct case when chargeback_amount > 0 then checkout_id else null end ) num_cbk
        , sum (chargeback_amount) cc_gmv_cbk
    from dim
    group by 1,2,3,4
        )
SELECT DISTINCT 
       *
FROM totals t1
    left join gmv_base t2 on t1.chargeback_date = t2.create_date
where date(chargeback_date) >= date('2021-08-01')



